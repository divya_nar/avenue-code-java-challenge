package OrderProduct.view;

import java.util.List;

/**
 * Created by divya on 5/6/17.
 */
public class OrderProductView {
    private Long orderId;
    private List<ProductView> products;
    private double orderTotal;

    public OrderProductView() {
    }

    public OrderProductView(Long orderId) {
        this.orderId = orderId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public List<ProductView> getProducts() {
        return products;
    }

    public void setProducts(List<ProductView> products) {
        this.products = products;
    }

    public double getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(double orderTotal) {
        this.orderTotal = orderTotal;
    }
}
