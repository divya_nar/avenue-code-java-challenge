package OrderProduct.Dao;

import OrderProduct.entity.Product;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by divya on 5/5/17.
 */
@Transactional
@Repository
public class ProductDaoImpl implements ProductDao{

    @PersistenceContext
    protected EntityManager productManager;

    @Override
    public List<Product> listProducts() {
        String hql = "FROM Product as prdt ORDER BY prdt.productId";
        return (List<Product>) productManager.createQuery(hql).getResultList();
    }

    @Override
    public Product getProduct(Long productId) {
        return productManager.find(Product.class,productId);
    }
}
