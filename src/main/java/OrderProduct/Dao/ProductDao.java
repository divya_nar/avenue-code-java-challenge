package OrderProduct.Dao;

import OrderProduct.entity.Product;

import java.util.List;

/**
 * Created by divya on 5/5/17.
 */
public interface ProductDao {
    List<Product> listProducts();

    Product getProduct(Long productId);

}
