package OrderProduct.Dao;

import OrderProduct.Controller.OrderController;
import OrderProduct.entity.OrderProduct;
import OrderProduct.entity.Orders;
import OrderProduct.entity.Product;
import OrderProduct.exception.OrderException;
import OrderProduct.view.OrderProductView;
import OrderProduct.view.ProductView;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author divya
 */
@Repository("userDao")
@Transactional
public class OrderDaoImpl implements OrderDao {
    private static final Logger LOG = LoggerFactory.getLogger(OrderController.class);
    @PersistenceContext
    private EntityManager orderManager;

    @Autowired
    ProductDao productDao;

    @Override
    public Orders createOrder(List<ProductView> prodList) {

        double cost = 0;
        Orders newOrders = new Orders();

        for (ProductView product : prodList) {
            Product product1 = productDao.getProduct(product.getId());
            cost = cost + (product1.getPrice() * product.getQuantity());
            if (product1 != null) {
                OrderProduct orderProduct = new OrderProduct();
                orderProduct.setOrder(newOrders);
                orderProduct.setProduct(product1);
                orderProduct.setQuantity(product.getQuantity());
                newOrders.getOrderProducts().add(orderProduct);
            }
            else{
                LOG.error("Invalid product with the order: " , product.getId());
                LOG.error("Order id : " ,newOrders.getOrderId());
            }
        }

        newOrders.setOrderTotal(cost);
        orderManager.persist(newOrders);

        return newOrders;
    }


    @Override
    public Orders updateOrder(OrderProductView order) {

        double cost = 0;
        Orders newOrders = new Orders();

        List<ProductView> productViews = order.getProducts();
        for (ProductView product : productViews) {
            Product product1 = productDao.getProduct(product.getId());
            cost = cost + (product1.getPrice() * product.getQuantity());
            if (product1 != null) {
                OrderProduct orderProduct = new OrderProduct();
                orderProduct.setOrder(newOrders);
                orderProduct.setProduct(product1);
                orderProduct.setQuantity(product.getQuantity());
                newOrders.getOrderProducts().add(orderProduct);
            }
            else{
                LOG.error("Invalid product with the order: " , product.getId());
                LOG.error("Order id : " ,newOrders.getOrderId());
            }
        }
        newOrders.setOrderId(order.getOrderId());
        newOrders.setOrderTotal(cost);
        orderManager.merge(newOrders);
        LOG.info("Order updated succesfully for orderId :",newOrders.getOrderId());
        return newOrders;
    }

    @Override
    public List<OrderProductView> listOrders() throws OrderException {
        Query q = orderManager.createNativeQuery("select o.orderid, o.ordertotal, p.productid, p.productname, p.price,p.productcategory,op.quantity" +
                " from orders o INNER JOIN orders_products op on o.orderid = op.orderid INNER JOIN product p on op.productid = p.productid");
        List<Object[]> resultList = q.getResultList();

        if (resultList == null || resultList.isEmpty()) {
            LOG.error("No orders available");
            throw new OrderException("No Order available");

        }

        Map<Long, OrderProductView> orderProductMap = new HashMap<>();
        for (Object[] a : resultList) {

            BigInteger orderId = (BigInteger) a[0];
            OrderProductView orderProductView = new OrderProductView();
            List<ProductView> productViews = Lists.newArrayList();

            if (orderProductMap.containsKey(orderId.longValue())) {
                orderProductView = orderProductMap.get(orderId.longValue());
                productViews = orderProductView.getProducts();
            }

            ProductView product = new ProductView();
            BigInteger productId = (BigInteger)(a[2]);
            product.setId(productId.longValue());
            product.setName(String.valueOf(a[3]));
            product.setPrice((double) a[4]);
            product.setCategory((String)a[5]);
            product.setQuantity((int) a[6]);
            orderProductView.setOrderId(orderId.longValue());
            orderProductView.setOrderTotal((double) a[1]);
            productViews.add(product);
            orderProductView.setProducts(productViews);
            orderProductMap.put(orderProductView.getOrderId(), orderProductView);
        }
        LOG.info("Order List fetched successfully");
        return orderProductMap.entrySet().stream().map(e -> e.getValue()).collect(Collectors.toList());
    }

    @Override
    public OrderProductView getOrderById(long orderId) throws OrderException {

        Query q = orderManager.createNativeQuery("select o.orderid, o.ordertotal, p.productid, p.productname, p.price,p.productcategory,op.quantity" +
                " from orders o INNER JOIN orders_products op on o.orderid = op.orderid INNER JOIN product p on op.productid = p.productid" +
                " where o.orderId =" + orderId);

        List<Object[]> resultList = q.getResultList();

        if (resultList == null || resultList.isEmpty()) {
            LOG.error("No order available for order id :" , orderId);
            throw new OrderException(String.format("No Order found for the given orderId =%s", orderId));
        }

        Map<Long, OrderProductView> orderProductMap = new HashMap<>();
        for (Object[] a : resultList) {

            OrderProductView orderProductView = new OrderProductView();
            List<ProductView> productViews = Lists.newArrayList();

            if (orderProductMap.containsKey(orderId)) {
                orderProductView = orderProductMap.get(orderId);
                productViews = orderProductView.getProducts();
            }

            ProductView product = new ProductView();
            BigInteger productId = (BigInteger)(a[2]);
            product.setId(productId.longValue());
            product.setName(String.valueOf(a[3]));
            product.setPrice((double) a[4]);
            product.setCategory((String)a[5]);
            product.setQuantity((int) a[6]);
            orderProductView.setOrderId(orderId);
            orderProductView.setOrderTotal((double) a[1]);
            productViews.add(product);
            orderProductView.setProducts(productViews);
            orderProductMap.put(orderProductView.getOrderId(), orderProductView);

        }
        LOG.info("Order fetched successfully for orderId : " ,orderId);
        return orderProductMap.get(orderId);
    }

}
