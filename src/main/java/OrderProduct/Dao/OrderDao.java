package OrderProduct.Dao;

import OrderProduct.entity.Orders;
import OrderProduct.exception.OrderException;
import OrderProduct.view.OrderProductView;
import OrderProduct.view.ProductView;

import java.util.List;

/**
 * @author divya
 */
public interface OrderDao {

    /**
     * Create a order with the given product list.
     * @param prodList
     * @return
     */
    Orders createOrder(List<ProductView> prodList);

    /**
     * Update the order with the new product list.
     * @param order
     * @return
     */
    Orders updateOrder(OrderProductView order);

    /**
     * List all the orders
     * @return
     */
    List<OrderProductView> listOrders() throws OrderException;

    /**
     * Get the order by the given orderId.
     * @param orderId
     * @return
     */
    OrderProductView getOrderById(long orderId) throws OrderException;

}
