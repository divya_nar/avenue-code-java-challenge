package OrderProduct.entity;

import org.hibernate.engine.profile.Fetch;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by divya on 5/6/17.
 */
@Entity
@Table(name = "Orders_Products")
@AssociationOverrides({
        @AssociationOverride(name = "primaryKey.order",
                joinColumns = @JoinColumn(name = "orderId")),
        @AssociationOverride(name = "primaryKey.product",
                joinColumns = @JoinColumn(name = "productId")) ,
})
public class OrderProduct implements Serializable {

    private OrderProductId primaryKey = new OrderProductId();
    private int quantity;

    public OrderProduct() {
    }

    @EmbeddedId
    public OrderProductId getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(OrderProductId primaryKey) {
        this.primaryKey = primaryKey;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Transient
    public Orders getOrder() {
        return getPrimaryKey().getOrder();
    }

    public void setOrder(Orders order) {
        getPrimaryKey().setOrder(order);
    }

    @Transient
    public Product getProduct() {
        return getPrimaryKey().getProduct();
    }

    public void setProduct(Product product) {
        getPrimaryKey().setProduct(product);
    }

    @Override
    public String toString() {
        return "OrderProduct{" +
                "primaryKey=" + primaryKey +
                ", quantity=" + quantity +
                '}';
    }
}
