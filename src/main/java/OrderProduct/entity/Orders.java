package OrderProduct.entity;


import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by divya on 5/6/17.
 */
@Entity
@Table(name = "orders")
public class Orders implements Serializable{

    private Long orderId;

    private double orderTotal;

    private List<OrderProduct> orderProducts = new ArrayList<>();

    public Orders() {
    }

    public Orders(Long orderId, double orderTotal, List<OrderProduct> orderProducts) {
        this.orderId = orderId;
        this.orderTotal = orderTotal;
        this.orderProducts = orderProducts;
    }

    /*@ManyToMany(cascade = CascadeType.ALL)
        @JoinTable(name = "Order_Products", joinColumns = { @JoinColumn(name = "orderId") }, inverseJoinColumns = { @JoinColumn(name = "productId") })
        private List<Product> products;*/
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "orderId", unique = true)
    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    @Column(name = "orderTotal", nullable = false, length = 10)
    public double getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(double orderTotal) {
        this.orderTotal = orderTotal;
    }


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "primaryKey.order", cascade=CascadeType.ALL)
    public List<OrderProduct> getOrderProducts() {
        return orderProducts;
    }

    public void setOrderProducts(List<OrderProduct> orderProducts) {
        this.orderProducts = orderProducts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Orders orders = (Orders) o;

        if (Double.compare(orders.orderTotal, orderTotal) != 0) return false;
        if (orderId != null ? !orderId.equals(orders.orderId) : orders.orderId != null) return false;
        return orderProducts != null ? orderProducts.equals(orders.orderProducts) : orders.orderProducts == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = orderId != null ? orderId.hashCode() : 0;
        temp = Double.doubleToLongBits(orderTotal);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (orderProducts != null ? orderProducts.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Orders{" +
                "orderId=" + orderId +
                ", orderTotal=" + orderTotal +
                ", orderProducts=" + orderProducts +
                '}';
    }
}
