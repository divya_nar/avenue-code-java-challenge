package OrderProduct.Controller;

import OrderProduct.Dao.OrderDao;
import OrderProduct.entity.Orders;
import OrderProduct.exception.OrderException;
import OrderProduct.service.OrderService;
import OrderProduct.view.OrderProductView;
import OrderProduct.view.ProductView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author divya
 */
@RestController
@RequestMapping(value = "/order")
public class OrderController {
    private static final Logger LOG = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;

    @RequestMapping(value = "/add", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createOrder(@RequestBody List<ProductView> products) throws OrderException {
        LOG.info("Creating an order for the products={}", products);
        Orders order = orderService.createOrder(products);
        String message;
        if (order != null) {
            message = "Orders Created with id : " + order.getOrderId() + "total is " + order.getOrderTotal();
            LOG.info("Order Created Successfully with order id : ", order.getOrderId());
            LOG.info("order total is : ", order.getOrderTotal());

        } else {
            message = "Orders Could not be placed";
            LOG.error(message);
            throw new OrderException(message);

        }
        return new ResponseEntity(message, HttpStatus.OK);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public List<OrderProductView> listOrder() throws OrderException {
        LOG.info("Fetching the order list");
        return orderService.listOrders();
    }

    @RequestMapping(value = "/{orderId}", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public OrderProductView getOrderById(@PathVariable("orderId") long orderId) throws OrderException {
        LOG.info("Fetching order for  :", orderId);
        return orderService.getOrderById(orderId);
    }

    @RequestMapping(value = "/{orderId}", method = RequestMethod.PUT,produces = MediaType.APPLICATION_JSON_VALUE)
    public Orders updateOrder(@PathVariable("orderId") long orderId,
                              @RequestBody OrderProductView orderProductView) throws OrderException {
        LOG.info("Updating the order with order id : ", orderId);
        if (orderId != orderProductView.getOrderId()) {
            throw new OrderException("Invalid request body");
        }
        return orderService.updateOrder(orderProductView);
    }
}
