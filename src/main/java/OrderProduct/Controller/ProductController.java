package OrderProduct.Controller;

import OrderProduct.entity.Product;
import OrderProduct.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by divya on 5/5/17.
 */
@RestController
@RequestMapping("/product")
public class ProductController {
    private static final Logger LOG = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    ProductService productService;

    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Product> getProducts() throws Exception {
        LOG.info("Getting the list of products");
        List<Product> products = productService.listProducts();
        if (products == null || products.isEmpty()) {
            LOG.error("There are no products");
            throw new Exception("There are no products");
        }
        return products;
    }

    @RequestMapping(value = "/get/{productId}", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public Product getProductById(@PathVariable("productId") long productId) throws Exception {
        LOG.info("Getting the product information for productId={}",productId);
        Product product = productService.getProduct(productId);
        if (product != null) {
            return product;
        }
        LOG.error("Product does not exist for the given productId :",productId);
        throw new Exception("Product does not exist for the given productId");
    }
}
