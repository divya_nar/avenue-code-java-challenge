package OrderProduct.service;

import OrderProduct.Dao.OrderDao;
import OrderProduct.entity.Orders;
import OrderProduct.exception.OrderException;
import OrderProduct.view.OrderProductView;
import OrderProduct.view.ProductView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by divya on 5/8/17.
 */
@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    OrderDao orderDao;
    @Override
    public Orders createOrder(List<ProductView> prodList) {
        return orderDao.createOrder(prodList);
    }

    @Override
    public Orders updateOrder(OrderProductView order) {
        return orderDao.updateOrder(order);
    }

    @Override
    public List<OrderProductView> listOrders() throws OrderException {
        return orderDao.listOrders();
    }

    @Override
    public OrderProductView getOrderById(long orderId) throws OrderException {
        return orderDao.getOrderById(orderId);
    }
}
