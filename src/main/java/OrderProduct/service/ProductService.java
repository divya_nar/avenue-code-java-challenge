package OrderProduct.service;

import OrderProduct.entity.Product;

import java.util.List;

/**
 * Created by divya on 5/6/17.
 */
public interface ProductService {
  /**
   * List the products
   * @return
   */
  List<Product> listProducts();

  /**
   *  Get the Product using the productId
    * @param productId
   * @return
   */
  Product getProduct(long productId);
}
