package OrderProduct.service;

import OrderProduct.Dao.ProductDao;
import OrderProduct.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by divya on 5/6/17.
 */
@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductDao productDao;

    @Override
    public List<Product> listProducts() {
        return productDao.listProducts();

    }

    @Override
    public Product getProduct(long productId)
    {
        return productDao.getProduct(productId);
    }
}
