package OrderProduct.service;

import OrderProduct.entity.Orders;
import OrderProduct.exception.OrderException;
import OrderProduct.view.OrderProductView;
import OrderProduct.view.ProductView;

import java.util.List;

/**
 * Created by divya on 5/8/17.
 */
public interface OrderService {
    Orders createOrder(List<ProductView> prodList);
    Orders updateOrder(OrderProductView order);
    List<OrderProductView> listOrders() throws OrderException;
    OrderProductView getOrderById(long orderId) throws OrderException;

}
