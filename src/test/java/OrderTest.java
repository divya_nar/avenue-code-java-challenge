import OrderProduct.Controller.OrderController;
import OrderProduct.entity.Orders;
import OrderProduct.service.OrderService;
import OrderProduct.view.OrderProductView;
import OrderProduct.view.ProductView;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by divya on 5/8/17.
 */
public class OrderTest {
    private MockMvc mockMvc;

    @Mock
    private OrderService orderService;

    @InjectMocks
    private OrderController orderController;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(orderController)
                .build();
    }
    @Test
    public void getOrderbyIdSuccess() throws Exception {
        OrderProductView orderProductView = new OrderProductView();
        orderProductView.setOrderId(Long.valueOf(25));
        when(orderService.getOrderById(25)).thenReturn(orderProductView);
        mockMvc.perform(get("/order/{id}", 25))
                .andExpect(status().isOk())
                .andExpect(jsonPath("orderId", is(25)));
        verify(orderService, times(1)).getOrderById(25);
        verifyNoMoreInteractions(orderService);
    }

    @Test
    public void listOrdersSuccess() throws Exception {
        List<OrderProductView> orderList = Arrays.asList(
                new OrderProductView(Long.valueOf(25)));
        orderList.get(0).setOrderTotal(170.0);
        when(orderService.listOrders()).thenReturn(orderList);
        mockMvc.perform(get("/order/list"))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].orderId", is(25)))
                .andExpect(jsonPath("$[0].orderTotal", is(170.0)));
        verify(orderService, times(1)).listOrders();
        verifyNoMoreInteractions(orderService);
    }

    @Test
    public void createOrder()throws Exception{
    List<ProductView> products = Arrays.asList(new ProductView(Long.valueOf(1),"shampoo","Beauty",10,2),
            new ProductView(Long.valueOf(2),"Soap","Beauty",12,1));
    Orders order = new Orders();
    order.setOrderId(Long.valueOf(27));//next order id
    order.setOrderTotal(32.0);
        when(orderService.createOrder(products)).thenReturn(order);
        mockMvc.perform(post("/order/add")
                .content(new ObjectMapper().writeValueAsString(products))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200));

        verify(orderService, times(1)).createOrder(products);
        verifyNoMoreInteractions(orderService);


    }
}
