import OrderProduct.Controller.ProductController;
import OrderProduct.entity.Product;
import OrderProduct.service.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
/**
 * Created by divya on 5/8/17.
 */
public class ProductTest {
    private MockMvc mockMvc;

    @Mock
    private ProductService productService;

    @InjectMocks
    private ProductController productController;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(productController)
                .build();
    }

    @Test
    public void getProductbyIdSuccess() throws Exception {
        Product prod = new Product();
        prod.setProductId(1);
        prod.setProductName("Shampoo");
        when(productService.getProduct(1)).thenReturn(prod);
        mockMvc.perform(get("/product/get/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("productId", is(1)))
                .andExpect(jsonPath("productName", is(prod.getProductName())));
        verify(productService, times(1)).getProduct(1);
        verifyNoMoreInteractions(productService);
    }

    @Test
    public void listProductsuccess() throws Exception {
        List<Product> productList = Arrays.asList(
                new Product(Long.valueOf(1), "Shampoo"),
                new Product(Long.valueOf(2), "soap"));

        when(productService.listProducts()).thenReturn(productList);

        mockMvc.perform(get("/product/list"))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].productId", is(1)))
                .andExpect(jsonPath("$[0].productName", is("Shampoo")))
                .andExpect(jsonPath("$[1].productId", is(2)))
                .andExpect(jsonPath("$[1].productName", is("soap")));

        verify(productService, times(1)).listProducts();
        verifyNoMoreInteractions(productService);
    }

}
